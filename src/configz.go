package main

import (
	"os"
)

type Config struct {
	serviceName string
	serviceMode string
	servicePort string
	dotKeys     string
	idRsa       string
	idRsaPub    string
	dotLogs     string
}

type KafkaConfig struct {
	topic         string
	cgroup        string
	kafkaAddr     string
	zookeeperAddr string
}

type TopicConfig struct {
	aplos string
}

var config = Config{
	serviceName: getEnv("SERVICE_NAME", "aplosapi"),
	serviceMode: getEnv("SERVICE_MODE", "DEV"),
	servicePort: getEnv("SERVICE_PORT", "8761"),
	dotKeys:     getEnv("DOT_KEYS", ".keys"),
	idRsa:       getEnv("ID_RSA", ".keys/id_rsa"),
	idRsaPub:    getEnv("ID_RSA_PUB", ".keys/id_rsa.pub"),
	dotLogs:     getEnv("DOT_LOGS", ".logs"),
}

var kafkaConfig = KafkaConfig{
	topic:         getEnv("KAFKA_TOPIC", "aplosapi"),
	cgroup:        getEnv("KAFKA_CGROUP", "aplosapig"),
	kafkaAddr:     getEnv("KAFKA_ADDR", "localhost:9042"),
	zookeeperAddr: getEnv("ZOOKEEPER_ADDR", "localhost:2181"),
}

var topicConfig = TopicConfig{
	aplos: getEnv("APLOS_TOPIC", "aplos"),
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}

	return fallback
}
