package main

func main() {
	// setup logging
	initLogz()

	// init kafka
	initKafkaz()

	// init rest api
	initHttpz()
}
