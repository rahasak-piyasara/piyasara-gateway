package main

import (
	"github.com/Shopify/sarama"
	"github.com/wvanbergen/kafka/consumergroup"
	"log"
	"os"
	"strings"
	"time"
)

type Kmsg struct {
	Topic string `json:"topic"`
	Uid   string `json:"uid"`
	Msg   string `json:"msg"`
}

// channel to publish kafka messages
var kchan = make(chan Kmsg, 10)

func initKafkaz() {
	// consuner
	cg := initConzumer()
	go conzume(cg)

	// producer
	pr := initProduzer()
	go produze(pr)
}

func initConzumer() *consumergroup.ConsumerGroup {
	// consumer config
	config := consumergroup.NewConfig()
	config.Offsets.Initial = sarama.OffsetOldest
	config.Offsets.ProcessingTimeout = 10 * time.Second

	// join to consumer group
	zookeeperConn := strings.Split(kafkaConfig.zookeeperAddr, ",")
	cg, err := consumergroup.JoinConsumerGroup(kafkaConfig.cgroup,
		[]string{kafkaConfig.topic},
		zookeeperConn,
		config)
	if err != nil {
		log.Printf("ERROR: fail init consumer: %s", err.Error())
		os.Exit(1)
	}

	log.Printf("INFO: done init consumer: %s", kafkaConfig.topic)

	return cg
}

func initProduzer() sarama.SyncProducer {
	// producer config
	config := sarama.NewConfig()
	config.Producer.Retry.Max = 5
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Return.Successes = true

	// sync producer
	kafkaConn := strings.Split(kafkaConfig.kafkaAddr, ",")
	pr, err := sarama.NewSyncProducer(
		kafkaConn,
		config)
	if err != nil {
		log.Printf("ERROR: fail init producer: %s", err.Error())
		os.Exit(1)
	}

	log.Printf("INFO: done init producer")

	return pr
}

func conzume(cg *consumergroup.ConsumerGroup) {
	for {
		select {
		case msg := <-cg.Messages():
			// messages coming through chanel
			// only take messages from subscribed topic
			if msg.Topic != kafkaConfig.topic {
				continue
			}

			log.Printf("INFO: message received topic: %s, msg: %s", msg.Topic, msg.Value)

			// commit to zookeeper that message is read
			// this prevent read message multiple times after restart
			err := cg.CommitUpto(msg)
			if err != nil {
				log.Printf("ERROR: fail commit zookeeper: %s", err.Error())
			}
		}
	}
}

func produze(pr sarama.SyncProducer) {
	for {
		select {
		case kmsg := <-kchan:
			// received kafka message to send
			// publish sync
			msg := &sarama.ProducerMessage{
				Topic: kmsg.Topic,
				Value: sarama.StringEncoder(kmsg.Msg),
			}
			p, o, err := pr.SendMessage(msg)
			if err != nil {
				log.Printf("ERROR: fail to publish: %s", err.Error())
			}

			log.Printf("INFO: published message: %s, uid: %s, partition: %d, offset: %d, topic: %s",
				kmsg.Msg, kmsg.Uid, p, o, kmsg.Topic)
		}
	}
}
